package exam;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ifast.face.service.FaceEngineService;
 
/**

 */
public class DrawFace extends TmallApplicationTests { 
    
	@Autowired
	private FaceEngineService faceEngineService;
	
    @Test
    public void createSmart(){
    	//recordCamera("output.MP4",25);
    	try {
			recordCamera("rtmp://192.168.0.182/oflaDemo/hello",30);
			Thread.currentThread().join();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    //private static String url = "http://admin:admin@192.168.0.53:8182";// 摄像头访问地址
    //private static String url = "rtmp://58.200.131.2:1935/livetv/hunantv";// 摄像头访问地址
    //private static String url = "http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8";
    private static String url = "rtmp://58.200.131.2:1935/livetv/gxtv";
    Java2DFrameConverter converter = new Java2DFrameConverter();
    public void recordCamera(String outputFile, double frameRate)
			throws Exception, InterruptedException, org.bytedeco.javacv.FrameRecorder.Exception {
		
		//FrameGrabber grabber = FrameGrabber.createDefault(0);//本机摄像头默认0，这里使用javacv的抓取器，至于使用的是ffmpeg还是opencv，请自行查看源码
		FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault(url);
		grabber.setOption("rtsp_transport", "tcp");
		grabber.start();//开启抓取器

		Frame imgFrame = grabber.grab();
		int width = imgFrame.imageWidth;
		int height = imgFrame.imageHeight;
	
		FrameRecorder recorder = FrameRecorder.createDefault(outputFile, width, height);
		recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264); // avcodec.AV_CODEC_ID_H264，编码
		recorder.setFormat("flv");//封装格式，如果是推送到rtmp就必须是flv封装格式
		recorder.setFrameRate(frameRate);
		recorder.setOption("codec"," copy");
		//recorder.setInterleaved(true);
		recorder.start();//开启录制器
		long startTime=0;
		long videoTS=0;
		//CanvasFrame frame = new CanvasFrame("camera", CanvasFrame.getDefaultGamma() / grabber.getGamma());
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setAlwaysOnTop(true);
		Frame img = null;
		int counts = 0;
		while ((img = grabber.grab()) != null) {
			 
			//frame.showImage(img);
			if (startTime == 0) {
				startTime = System.currentTimeMillis();
			}  
			System.out.println("---====----");
			videoTS = 1000 * ( System.currentTimeMillis() - startTime);
			recorder.setTimestamp(videoTS);
			//System.out.println(img);
			BufferedImage bImg = converter.convert(img);
			if(bImg == null){
				continue;
			}
//			if(counts == 40){
//				counts = 0;
				bImg = faceEngineService.drowFace(bImg);
//			} 
			counts++;
			recorder.record(converter.convert(bImg));
			//Thread.sleep(10);
		}
		//frame.dispose();
		recorder.stop();
		recorder.release();
		grabber.stop(); 
	}
}