package exam;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.SparkSession;

import com.beust.jcommander.internal.Maps;

import scala.Tuple2;

public final class JavaWordCount {
	private static final Pattern SPACE = Pattern.compile(" ");

	public static void main(String[] args) throws Exception {
		args = "".split(",");
		if (args.length < 1) {
			System.err.println("Usage: JavaWordCount <file>");
			System.exit(1);
		}

		SparkSession spark = SparkSession.builder().master("local").appName("JavaWordCount").getOrCreate();

		JavaRDD<String> lines = spark.read().textFile("F:/test.txt").javaRDD();

		System.out.println();
		System.out.println("-------------------------------------------------------");
		System.out.println(lines.count());

		JavaRDD<String> words = lines.flatMap(str -> Arrays.asList(SPACE.split(str)).iterator());

		JavaPairRDD<String, Integer> ones = words.mapToPair(str -> new Tuple2<String, Integer>(str, 1));

		JavaPairRDD<String, Integer> counts = ones.reduceByKey((Integer i1, Integer i2) -> (i1 + i2));

		JavaPairRDD<Integer, String> temp = counts.mapToPair(tuple -> new Tuple2<Integer, String>(tuple._2, tuple._1));

		JavaPairRDD<String, Integer> sorted = temp.sortByKey(false)
				.mapToPair(tuple -> new Tuple2<String, Integer>(tuple._2, tuple._1));

		System.out.println();
		System.out.println("-------------------------------------------------------");
		System.out.println(sorted.count());

		// List<Tuple2<String, Integer>> output = sorted.collect();

		 //List<Tuple2<String, Integer>> output = sorted.take(10);
		WordCountComparator cop = new WordCountComparator();
		List<Tuple2<String, Integer>> output = sorted.top(10,cop);
		Map<String, Object> result = Maps.newHashMap();
		for (Tuple2<String, Integer> tuple : output) {
			 result.put(tuple._1(), tuple._2());
		}
	    System.out.println(result);
		spark.stop();
	}
	static class WordCountComparator implements Comparator<Tuple2<String, Integer>>, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(Tuple2<String, Integer> o1, Tuple2<String, Integer> o2) {
		    // TODO Auto-generated method stub
		    return o2._2()-o1._2();
		}
	}
}