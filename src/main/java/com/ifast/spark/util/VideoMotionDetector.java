package com.ifast.spark.util;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.TaskContext;

import com.ifast.spark.domain.VideoEventData;

import cn.hutool.core.util.ImageUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VideoMotionDetector implements Serializable {
	 
	private static final long serialVersionUID = -6796300566981510371L;
	 
	public static VideoEventData detectMotion(String camId, Iterator<VideoEventData> frames, String outputDir,
			VideoEventData previousProcessedEventData) throws Exception { 
		 
		VideoEventData currentProcessedEventData = new VideoEventData();
		 
		if (previousProcessedEventData != null) {
			log.warn("cameraId=" + camId + " previous processed timestamp=" + previousProcessedEventData.getTimestamp());
		}

		// sort by timestamp
		ArrayList<VideoEventData> sortedList = new ArrayList<VideoEventData>();
		while (frames.hasNext()) {
			sortedList.add(frames.next());
		}
		sortedList.sort(Comparator.comparing(VideoEventData::getTimestamp));
		log.warn("cameraId=" + camId + " total frames=" + sortedList.size());

		// iterate and detect motion 
		for (VideoEventData eventData : sortedList) { 
			log.warn("保存图片数据：", eventData.toString());
			saveImage(eventData, outputDir); 
			currentProcessedEventData = eventData;
		}
		return currentProcessedEventData;
	} 
	 
	// Save image file
	private static void saveImage(VideoEventData ed, String outputDir) {
		if(StringUtils.isBlank(ed.getData())){
			return;
		}
		String imagePath = outputDir +File.separator+ ed.getCameraId() + "-T-" + ed.getTimestamp().getTime() + ".png";
		log.warn("Saving images to " + imagePath);
		  
		try {
			ImageIO.write(ImageUtil.toImage(ed.getData()),"png",new File(imagePath));
		} catch (Exception e) { 
			e.printStackTrace();
		}
	
	}
}
