package com.ifast.spark.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * Java Bean to hold JSON message
 *
 */
@Getter
@Setter
@ToString
public class VideoEventData implements Serializable { 
	private static final long serialVersionUID = 8212561153024908580L;
	private String cameraId;
	private Timestamp timestamp;
	private int rows;
	private int cols;
	private int type;
	private String data; 
}
