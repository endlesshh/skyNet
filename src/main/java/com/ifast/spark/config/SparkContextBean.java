package com.ifast.spark.config;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "spark")
@Getter
@Setter
public class SparkContextBean {

    private String sparkHome = ".";

    private String appName = "VideoStreamProcessor";

    private String master = "local";

    @Bean
    @ConditionalOnMissingBean(SparkConf.class)
    public SparkConf sparkConf() throws Exception {
	   SparkConf conf = new SparkConf().setAppName(appName).setMaster(master);
	   return conf;
    }

    @Bean
    @ConditionalOnMissingBean(JavaSparkContext.class)
    public JavaSparkContext javaSparkContext() throws Exception {
	   return new JavaSparkContext(sparkSession().sparkContext());
    } 
    
    @Bean
    @ConditionalOnMissingBean(SparkSession.class)
    public SparkSession sparkSession() throws Exception{ 
    	return SparkSession.builder().config(sparkConf()).getOrCreate();
    } 
    
}