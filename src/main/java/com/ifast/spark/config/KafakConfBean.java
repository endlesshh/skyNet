package com.ifast.spark.config;

import java.io.Serializable;
import java.util.Map;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;

@Configuration
@Getter
@Setter
public class KafakConfBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1155330019200241522L;

	@Value("${kafka.bootstrap.servers}")
    private String servers;
	
	@Value("${kafka.acks}")
    private String acks;
	
	@Value("${kafka.retries}")
    private String retries;
	
	@Value("${kafka.batch.size}")
    private String size;
	
	@Value("${kafka.linger.ms}")
    private String ms;
	
	@Value("${kafka.compression.type}")
    private String type;
	
	@Value("${kafka.max.request.size}")
    private String requestSize;
	
	@Value("${kafka.topic}")
    private String topic;
    
	@Value("${kafka.max.partition.fetch.bytes}")
    private String partitionBytes;
	
	@Value("${kafka.max.poll.records}")
    private String records;
	
	@Value("${camera.url}")
    private String cameraUrl;
	
	@Value("${camera.id}")
    private String cameraId;
	
	@Value("${processed.output.dir}")
    private String dir;
	
	

    @Bean
    @ConditionalOnMissingBean(Producer.class)
    public Producer<String,String> KafkaConf(){
    	Map<String,Object> properties = Maps.newHashMap();
    	properties.put("bootstrap.servers",servers);
		properties.put("acks",acks);
		properties.put("retries",retries);
		properties.put("batch.size", size);
		properties.put("linger.ms",ms);
		properties.put("max.request.size", requestSize);
		properties.put("compression.type", type);
		properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		return new KafkaProducer<String, String>(properties);
    } 
}