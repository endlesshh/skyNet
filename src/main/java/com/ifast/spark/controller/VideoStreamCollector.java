package com.ifast.spark.controller;

import org.apache.kafka.clients.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ifast.spark.config.KafakConfBean;
import com.ifast.spark.service.VideoEventGenerator;
import com.ifast.spark.service.VideoStreamProcessor;

import lombok.extern.slf4j.Slf4j;

/**
 *  Class to configure Kafka Producer and connect to Video camera url.
 */
@RestController
@Slf4j
public class VideoStreamCollector {

	@Autowired
	private KafakConfBean kafakConfBean;
	
	@Autowired
	private VideoStreamProcessor videoStreamProcessor;
	
	@RequestMapping("/demo/processer")
	public void processer() {
		try {
			videoStreamProcessor.processor();
		} catch (Exception e) { 
			e.printStackTrace();
		}
	} 
	
	@RequestMapping("/demo/collector")
	public void collector() {
		generateIoTEvent(kafakConfBean.KafkaConf(),kafakConfBean.getTopic(),kafakConfBean.getCameraId(),kafakConfBean.getCameraUrl());
	} 

	private static void generateIoTEvent(Producer<String, String> producer, String topic, String camId, String videoUrl){
		String[] urls = videoUrl.split(",");
		String[] ids = camId.split(",");
		if(urls.length != ids.length){
			//throw new Exception("There should be same number of camera Id and url");
		}
		log.info("Total urls to process "+urls.length);
		for(int i=0;i<urls.length;i++){
			Thread t = new Thread(new VideoEventGenerator(ids[i].trim(),urls[i].trim(),producer,topic));
			t.start();
		}
	}
}
