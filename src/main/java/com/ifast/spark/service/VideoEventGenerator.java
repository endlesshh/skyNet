package com.ifast.spark.service;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.util.Base64;

import javax.imageio.ImageIO;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import lombok.extern.slf4j.Slf4j;

/**
 * Class to convert Video Frame into byte array and generate JSON event using Kafka Producer.
 */
@Slf4j
public class VideoEventGenerator implements Runnable { 
	private String cameraId;
	private String url;
	private Producer<String, String> producer;
	private String topic;
	
	public VideoEventGenerator(String cameraId, String url, Producer<String, String> producer, String topic) {
		this.cameraId = cameraId;
		this.url = url;
		this.producer = producer;
		this.topic = topic;
	}
	
	@Override
	public void run() { 
		try {
			generateEvent(cameraId,url,producer,topic);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	static Java2DFrameConverter converter = new Java2DFrameConverter();
	 
	private void generateEvent(String cameraId,String url,Producer<String, String> producer, String topic) throws Exception{

		@SuppressWarnings("resource")
		OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
	
		grabber.start(); // 开始获取摄像头数据  
        Frame frame = null;
		while ((frame = grabber.grab()) != null) {
			//resize image before sending
			if (frame == null || frame.image == null) {
	            continue;
	        }
			BufferedImage mat = converter.convert(frame);
			
			 
	        String timestamp = new Timestamp(System.currentTimeMillis()).toString();
			JsonObject obj = new JsonObject();
			obj.addProperty("cameraId",cameraId);
	        obj.addProperty("timestamp", timestamp);
	        obj.addProperty("rows", mat.getHeight());
	        obj.addProperty("cols", mat.getWidth());
	        obj.addProperty("type", mat.getType());
	        
	        try(ByteArrayOutputStream out = new ByteArrayOutputStream();){ 
				boolean flag = ImageIO.write(mat, "png", out);
				byte[] data = out.toByteArray();
				obj.addProperty("data", Base64.getEncoder().encodeToString(data));  
			} catch (Exception e) {
				e.printStackTrace();
			} 
	        String json = new Gson().toJson(obj);
	        
	        producer.send(new ProducerRecord<String, String>(topic,cameraId, json),new EventGeneratorCallback(cameraId));
	        log.info("生成数据： for cameraId={} timestamp={}",cameraId,timestamp);
		}      
		grabber.release(); 
	}
	
	private class EventGeneratorCallback implements Callback {
		private String camId;

		public EventGeneratorCallback(String camId) {
			super();
			this.camId = camId;
		}

		@Override
		public void onCompletion(RecordMetadata rm, Exception e) {
			if (rm != null) {
				log.info("cameraId="+ camId + " partition=" + rm.partition());
			}
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

}
