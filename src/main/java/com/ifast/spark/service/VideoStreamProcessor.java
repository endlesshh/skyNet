package com.ifast.spark.service;

import java.io.Serializable;
import java.util.Iterator;

import org.apache.spark.TaskContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.MapGroupsWithStateFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.KeyValueGroupedDataset;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.GroupState;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ifast.spark.domain.VideoEventData;
import com.ifast.spark.util.VideoMotionDetector;

import lombok.extern.slf4j.Slf4j;

/**
 * Class to consume incoming JSON messages from Kafka and process them using
 * Spark Structured Streaming.
 *
 */
@Service
@Slf4j
public class VideoStreamProcessor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6222811130366441310L;

	@Value("${kafka.bootstrap.servers}")
    private String servers;
	
	@Value("${kafka.acks}")
    private String acks;
	
	@Value("${kafka.retries}")
    private String retries;
	
	@Value("${kafka.batch.size}")
    private String size;
	
	@Value("${kafka.linger.ms}")
    private String ms;
	
	@Value("${kafka.compression.type}")
    private String type;
	
	@Value("${kafka.max.request.size}")
    private String requestSize;
	
	@Value("${kafka.topic}")
    private String topic;
    
	@Value("${kafka.max.partition.fetch.bytes}")
    private String partitionBytes;
	
	@Value("${kafka.max.poll.records}")
    private String records;
	
	@Value("${camera.url}")
    private String cameraUrl;
	
	@Value("${camera.id}")
    private String cameraId;
	
	@Value("${processed.output.dir}")
    private String dir;
	
	 
	@Autowired
	private SparkSession spark;
	
	public void processor() throws Exception {
		Thread t = new Thread(new Process(spark,servers,topic,partitionBytes,records,dir));
		t.start(); 
	}
	class Process implements Runnable,Serializable{ 
		/**
		 * 
		 */
		private static final long serialVersionUID = -1283124207427773542L;

		public Process(SparkSession spark,String servers,String topic,String partitionBytes,String records,String dir){
			this.spark= spark;
			this.servers= servers;
			this.topic= topic;
			this.partitionBytes=partitionBytes;
			this.records=records;
			this.dir =dir;    
		}
		
		private SparkSession spark;
		 
	    private String servers; 
	   
	    private String topic;
	    
	    private String partitionBytes;
		 
	    private String records; 
		 
	    private String dir;
		
		@Override
		public void run() {
			// directory to save image files with motion detected
			final String processedImageDir = dir;
			log.warn("Output directory for saving processed images is set to " + processedImageDir
					+ ". This is configured in processed.output.dir key of property file.");

			// create schema for json message
			StructType schema = DataTypes.createStructType(
					new StructField[] { DataTypes.createStructField("cameraId", DataTypes.StringType, true),
							DataTypes.createStructField("timestamp", DataTypes.TimestampType, true),
							DataTypes.createStructField("rows", DataTypes.IntegerType, true),
							DataTypes.createStructField("cols", DataTypes.IntegerType, true),
							DataTypes.createStructField("type", DataTypes.IntegerType, true),
							DataTypes.createStructField("data", DataTypes.StringType, true) });

			// Create DataSet from stream messages from kafka
			Dataset<VideoEventData> ds = spark.readStream().format("kafka")
					.option("kafka.bootstrap.servers", servers)
					.option("subscribe", topic)
					.option("kafka.max.partition.fetch.bytes", partitionBytes)
					.option("kafka.max.poll.records", records).load()
					.selectExpr("CAST(value AS STRING) as message")
					.select(functions.from_json(functions.col("message"), schema).as("json")).select("json.*")
					.as(Encoders.bean(VideoEventData.class));

			// key-value pair of cameraId-VideoEventData
			KeyValueGroupedDataset<String, VideoEventData> kvDataset = ds
					.groupByKey(new MapFunction<VideoEventData, String>() {
						 
						private static final long serialVersionUID = 5995373730313619664L;

						@Override
						public String call(VideoEventData value) throws Exception {
							return value.getCameraId();
						}
					}, Encoders.STRING());

			// process
			Dataset<VideoEventData> processedDataset = kvDataset.mapGroupsWithState(
					new MapGroupsWithStateFunction<String, VideoEventData, VideoEventData, VideoEventData>() {
					 
						private static final long serialVersionUID = -2981345989581023534L;

						@Override
						public VideoEventData call(String key, Iterator<VideoEventData> values,
								GroupState<VideoEventData> state) throws Exception {
							log.warn("CameraId={} PartitionId={}" ,key,TaskContext.getPartitionId());
							VideoEventData existing = null;
							// check previous state
							if (state.exists()) {
								existing = state.get();
							}
							// detect motion 
							VideoEventData processed = VideoMotionDetector.detectMotion(key, values, processedImageDir,
									existing);

							// update last processed
							if (processed != null) {
								state.update(processed);
							}
							return processed;
						}
					}, Encoders.bean(VideoEventData.class), Encoders.bean(VideoEventData.class));

			// start
			StreamingQuery query = processedDataset.writeStream().outputMode("update").format("console").start();
			// await
			try {
				query.awaitTermination();
			} catch (StreamingQueryException e) { 
				e.printStackTrace();
			} 
		}
		
	}
	 
}
