package com.ifast.vlcj;

import java.text.DecimalFormat;

import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.TrackType;

public class DirectHttp{
	private static String url = "http://admin:admin@192.168.0.53:8182";// 摄像头访问地址
    public static void main(String[] args) throws Exception {
       
        new NativeDiscovery().discover();	//自动搜索libvlc路径并初始化，这行代码一定要加，且libvlc要已经安装，否则会报错
        String options = formatHttpStream("127.0.0.1", 5555);

        MediaPlayerFactory factory = new MediaPlayerFactory();
        MediaPlayer mediaPlayer = factory.newHeadlessMediaPlayer();
        MediaPlayer mediaPlayer2 = factory.newHeadlessMediaPlayer();
        
        mediaPlayer.addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
            DecimalFormat df = new DecimalFormat("0.00");
            @Override
            public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media, String mrl) {
            	 
            }
            @Override
            public void playing(MediaPlayer mediaPlayer) {
                System.out.println("Playing...");
            }
            
            @Override
            public void buffering(MediaPlayer mediaPlayer, float newCache){
            	 
            }
            
            @Override
            public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
                // This escape sequence to reset the terminal window cursor back to
                // column zero will not work in the Eclipse console window and most
                // likely not work on Windows at all
               // System.out.println(df.format(newPosition * 100.0f) + "%" + "\u001b[0G");
              
            	
           	 
            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {
                System.out.println();
                System.out.println("Finished.");
                mediaPlayer.release();
                factory.release();
                try {
                    // Probably not required, but just in case there are any pending
                    // native buffers
                    Thread.sleep(1000);
                }
                catch(InterruptedException e) {
                }
                System.exit(0);
            }

            @Override
            public void error(MediaPlayer mediaPlayer) {
                System.out.println();
                System.out.println("Error.");
                mediaPlayer.release();
                factory.release();
                System.exit(1);
            }
            @Override
            public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {
                System.out.println("snapshotTaken(filename=" + filename + ")");
            }
        });

        mediaPlayer.prepareMedia(url);

        mediaPlayer.parseMedia();

        mediaPlayer.start();  
        

        System.out.println("Track Information before end: " + mediaPlayer.getTrackInfo());

        System.out.println("    UNKNOWN: " +  mediaPlayer.getTrackInfo(TrackType.UNKNOWN));
        System.out.println("      AUDIO: " +  mediaPlayer.getTrackInfo(TrackType.AUDIO));
        System.out.println("      VIDEO: " +  mediaPlayer.getTrackInfo(TrackType.VIDEO));
        System.out.println("       TEXT: " +  mediaPlayer.getTrackInfo(TrackType.TEXT));
        System.out.println("AUDIO+VIDEO: " +  mediaPlayer.getTrackInfo(TrackType.AUDIO, TrackType.VIDEO));
        Thread.currentThread().join();
        mediaPlayer.stop();

        mediaPlayer.release();
        factory.release();
    }
    private static String formatHttpStream(String serverAddress, int serverPort) {
        StringBuilder sb = new StringBuilder(60);
        sb.append(":sout=#duplicate{dst=std{access=http,mux=ts,");
        sb.append("dst=");
        sb.append(serverAddress);
        sb.append(':');
        sb.append(serverPort);
        sb.append("}}");
        return sb.toString();
    }
}