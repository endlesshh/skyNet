package com.ifast.vlcj;

import cn.hutool.core.thread.ThreadUtil;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;

public class HttpPushScreenTest {

	static MediaPlayerFactory mediaPlayerFactory;
	private static String url = "http://admin:admin@192.168.0.22:8182";// 摄像头访问地址
	static MediaPlayer mediaPlayer;
	static MediaPlayer saveMediaPlayer;
	static String options[] = new String[]{"--live-caching 0", "--avcodec-hr=vaapi_drm"};
	static String[] VLC_ARGS = {  "--vout", "dummy" }; 
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {

		new NativeDiscovery().discover();	//自动搜索libvlc路径并初始化，这行代码一定要加，且libvlc要已经安装，否则会报错
		
		mediaPlayerFactory = new MediaPlayerFactory(VLC_ARGS);
		mediaPlayer = mediaPlayerFactory.newEmbeddedMediaPlayer();
		//mediaPlayer.playMedia("screen:// ", getMediaOptions("127.0.0.1", 5888));
		mediaPlayer.playMedia(url, getMediaOptions("127.0.0.1", 5888));
	  
		
		// 创建一个HeadlessMediaPlayer ，在不需要展示视频画面的情况下，使用HeadlessMediaPlayer 是最合适的（尤其是在服务器环境下）
		saveMediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
		saveMediaPlayer.playMedia(url, options);	//开始播放视频，这里传入的是rtsp连接， 传入其他格式的链接也是可以的，网络链接、本地路径都行
		
		//开始播放之后，可以另起一个线程来获取视频帧 （这里使用的hutool框架来开启线程）
		 ThreadUtil.execAsync(()->{
	         while (true){
		         if (mediaPlayer.isPlaying()){
		        	 Thread.currentThread().sleep(1000);
		             System.out.println(saveMediaPlayer.getSnapshot());
		             // 具体计算逻辑省略
		         }
	         }
         });
		 
		Thread.currentThread().join();
	}

	private static String formatHttpStream(String serverAddress, int serverPort) {
		StringBuilder sb = new StringBuilder(100);

		sb.append(":sout=#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100}:duplicate{dst=std{access=http,mux=ts,");
		sb.append("dst=");
		sb.append(serverAddress);
		sb.append(':');
		sb.append(serverPort);
		sb.append("}}");
		return sb.toString();
	}

	private static String[] getMediaOptions(String serverAddress, int serverPort) {
		// return new String[] { formatHttpStream(serverAddress, serverPort),
		// ":screen-fps=30", ":screen-caching=100" };

		return new String[] { formatHttpStream(serverAddress, serverPort), ":--live-caching 0"};

	}

}