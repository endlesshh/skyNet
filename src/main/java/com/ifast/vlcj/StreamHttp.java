package com.ifast.vlcj;

import cn.hutool.core.thread.ThreadUtil;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

public class StreamHttp{
	private static String url = "http://admin:admin@192.168.0.22:8182";// 摄像头访问地址
    public static void main(String[] args) throws Exception {
    	new NativeDiscovery().discover();	//自动搜索libvlc路径并初始化，这行代码一定要加，且libvlc要已经安装，否则会报错
		
       
        String options = formatHttpStream("127.0.0.1", 5555);

        System.out.println("Streaming  to '" + options + "'");

        MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory(args);
        HeadlessMediaPlayer mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
        
        mediaPlayer.playMedia(url, options);
      //开始播放之后，可以另起一个线程来获取视频帧 （这里使用的hutool框架来开启线程）
		ThreadUtil.execAsync(()->{
	         while (true){
		         if (mediaPlayer.isPlaying()){ 
		             System.out.println(mediaPlayer.getSnapshot());
		             // 具体计算逻辑省略
		         }
	         }
        });
		 
        // Don't exit
        Thread.currentThread().join();
    }

    private static String formatHttpStream(String serverAddress, int serverPort) {
        StringBuilder sb = new StringBuilder(60);
        sb.append(":sout=#duplicate{dst=std{access=http,mux=ts,");
        sb.append("dst=");
        sb.append(serverAddress);
        sb.append(':');
        sb.append(serverPort);
        sb.append("}}");
        return sb.toString();
    }
}