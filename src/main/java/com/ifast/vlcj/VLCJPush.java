package com.ifast.vlcj;

import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

public class VLCJPush {
	private static Boolean isInterrupt = false;
	private static String url = "http://admin:admin@192.168.0.22:8182";// 摄像头访问地址
	private static HeadlessMediaPlayer mediaPlayer;
	private static String suffix = "/oflaDemo/helloOne";// 发布后的最后一级访问路径
	private static String serverAddress="192.168.0.182";// 发布后的地址(IP)
	private static int serverPort=1935;// 发布后的端口号
 	private static  MediaPlayerFactory mediaPlayerFactory;
	public static void main(String[] args) throws InterruptedException {
		new NativeDiscovery().discover();	//自动搜索libvlc路径并初始化，这行代码一定要加，且libvlc要已经安装，否则会报错
		
		// 创建播放器工厂
		mediaPlayerFactory = new MediaPlayerFactory();	//这样写的话则不展示视频图像， 要想展示图像的话则直接new MediaPlayerFactory()；
		// 创建一个HeadlessMediaPlayer ，在不需要展示视频画面的情况下，使用HeadlessMediaPlayer 是最合适的（尤其是在服务器环境下）
		mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
		
//		StreamTask run = new StreamTask(url, mediaPlayer, serverAddress, serverPort, suffix);
//		new Thread(run).start();
		
		System.out.println(url + "开始运行");
		try {

			// 不管摄像头的类型是什么，浏览器的网络协议就是http，所以全部按照http协议转换。
			String options = formatHttpStream(serverAddress, serverPort, suffix);

			// 此处针对不同的协议类型，发布的时候也需要附带不同的参数。
			if (url.startsWith("http")) {
				mediaPlayer.playMedia(url, options);
			} else if (url.startsWith("rtsp") | url.startsWith("rtp")) {
				mediaPlayer.playMedia(url, options, ":no-sout-rtp-sap", ":sout-ts-dts-delay=100",
						":no-sout-standard-sap", ":sout-all", ":sout-keep");
			}
		} catch (Exception e) {
			System.out.println(url + "发布失败，从阻塞中退出...");
		}
		
		while (true){
	         if (mediaPlayer.isPlaying()){ 
	             
	             System.out.println("=========");
	             // 具体计算逻辑省略
	         }
         }
	}

	static class StreamTask implements Runnable {
		private String url;// 摄像头访问地址
		private HeadlessMediaPlayer mediaPlayer;
		private String suffix;// 发布后的最后一级访问路径
		private String serverAddress;// 发布后的地址(IP)
		private int serverPort;// 发布后的端口号
		public StreamTask(){}
		public StreamTask(String url, HeadlessMediaPlayer mediaPlayer, String serverAddress, int serverPort,
				String suffix) {
			this.url = url;
			this.mediaPlayer = mediaPlayer;
			this.serverAddress = serverAddress;
			this.serverPort = serverPort;
			this.suffix = suffix;
		}

		@Override
		public void run() {

			System.out.println(url + "开始运行");
			try {

				// 不管摄像头的类型是什么，浏览器的网络协议就是http，所以全部按照http协议转换。
				String options = formatHttpStream(serverAddress, serverPort, suffix);

				// 此处针对不同的协议类型，发布的时候也需要附带不同的参数。
				if (url.startsWith("http")) {
					mediaPlayer.playMedia(url, options);
				} else if (url.startsWith("rtsp") | url.startsWith("rtp")) {
					mediaPlayer.playMedia(url, options, ":no-sout-rtp-sap", ":sout-ts-dts-delay=100",
							":no-sout-standard-sap", ":sout-all", ":sout-keep");
				}
			} catch (Exception e) {
				System.out.println(url + "发布失败，从阻塞中退出...");
			}

		}
	}

	private static String formatHttpStream(String serverAddress, int serverPort, String id) {
		StringBuilder sb = new StringBuilder(60);
		sb.append(":sout=#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100,scodec=none} :rtp{");
		sb.append("dst=");
		sb.append(serverAddress);
		sb.append(",port=");
		sb.append(serverPort);
		sb.append(",sap,name=");
		sb.append(id);
		sb.append("}").append(",:sout-ts-dts-delay=100").append(", :sout-all").append(", :sout-keep");
		return sb.toString();
	}

}
