package com.ifast.vlcj;
import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.binding.internal.libvlc_instance_t;
import uk.co.caprica.vlcj.binding.internal.libvlc_media_player_t;
import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;

/**
 * The most minimal test that uses the raw bindings rather than any higher level
 * framework provided by vlcj.
 * <p>
 * This is only used for testing.
 * <p>
 * Specify a media file as the only command-line argument.
 */
public class BareBonesTest{
	private static String url = "http://admin:admin@192.168.0.22:8182";// 摄像头访问地址
    
    public static void main(String[] args) throws Exception {
    	new NativeDiscovery().discover();	//自动搜索libvlc路径并初始化，这行代码一定要加，且libvlc要已经安装，否则会报错
		
    	LibVlc libvlc = LibVlc.INSTANCE;

        libvlc_instance_t instance = libvlc.libvlc_new(0, new String[] {});

        libvlc_media_player_t mediaPlayer = libvlc.libvlc_media_player_new(instance);
        libvlc_media_t media = libvlc.libvlc_media_new_path(instance, url);
        libvlc.libvlc_media_player_set_media(mediaPlayer, media);
        libvlc.libvlc_media_player_play(mediaPlayer);

        Thread.sleep(10000);

        libvlc.libvlc_media_player_stop(mediaPlayer);
        libvlc.libvlc_media_release(media);
        libvlc.libvlc_media_player_release(mediaPlayer);

        libvlc.libvlc_release(instance);

        System.exit(0);
    }
}