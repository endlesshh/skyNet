package com.ifast.hessian.service;

import java.util.List;

import com.ifast.hessian.domain.FaceSearchHassanResult;

public interface CheckFaceService {
	
	public List<FaceSearchHassanResult> toCheck(Long groupId);
	public boolean addToCache(Long groupId,Long userFaceInfoId);
	public void changePassRate(int passRate);
	
}
