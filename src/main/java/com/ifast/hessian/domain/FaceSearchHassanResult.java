package com.ifast.hessian.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author: ShiQiang
 * @Date: 2019年5月24日11:31:17
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FaceSearchHassanResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5874226845553682067L;
	private Long id;
    private String faceId;
    private String name;
    private Integer similarValue;
    private Integer age;
    private Integer gender;
    private byte[] img; 
    private String facePath;
    private String pathType;
}
