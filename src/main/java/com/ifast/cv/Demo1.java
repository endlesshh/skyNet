package com.ifast.cv;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class Demo1 {
	/**
	 * 按帧录制本机摄像头视频（边预览边录制，停止预览即停止录制）
	 * 
	 * @author 
	 * @param outputFile -录制的文件路径，也可以是rtsp或者rtmp等流媒体服务器发布地址
	 * @param frameRate - 视频帧率
	 * @throws Exception
	 * @throws InterruptedException
	 * @throws org.bytedeco.javacv.FrameRecorder.Exception
	 */
	public static void recordCamera(String outputFile, double frameRate)
			throws Exception, InterruptedException, org.bytedeco.javacv.FrameRecorder.Exception {
		Loader.load(opencv_objdetect.class);
		FrameGrabber grabber = FrameGrabber.createDefault(0);//本机摄像头默认0，这里使用javacv的抓取器，至于使用的是ffmpeg还是opencv，请自行查看源码
		//grabber.setOption("rtsp_transport", "tcp");
		grabber.start();//开启抓取器

		Frame imgFrame = grabber.grab();
		int width = imgFrame.imageWidth;
		int height = imgFrame.imageHeight;
	
		FrameRecorder recorder = FrameRecorder.createDefault(outputFile, width, height);
		recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264); // avcodec.AV_CODEC_ID_H264，编码
		recorder.setFormat("flv");//封装格式，如果是推送到rtmp就必须是flv封装格式
		recorder.setFrameRate(frameRate);
		//recorder.setInterleaved(true);
		recorder.start();//开启录制器
		long startTime=0;
		long videoTS=0;
		CanvasFrame frame = new CanvasFrame("camera", CanvasFrame.getDefaultGamma() / grabber.getGamma());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setAlwaysOnTop(true);
		Frame img = null;
		while (frame.isVisible() && (img = grabber.grab()) != null) {
			 
			frame.showImage(img);
			if (startTime == 0) {
				startTime = System.currentTimeMillis();
			} 
			//Frame newImg = img.clone();
			//System.out.println(img);
			videoTS = 1000 * ( System.currentTimeMillis() - startTime);
			recorder.setTimestamp(videoTS);
			recorder.record(img);
			Thread.sleep(40);
		}
		frame.dispose();
		recorder.stop();
		recorder.release();
		grabber.stop();
	
	}
	public BufferedImage copyImg(BufferedImage img){  
		BufferedImage checkImg = new BufferedImage(img.getWidth(),img.getHeight(),img.getType() == 0 ? 5 : img.getType());
		checkImg.setData(img.getData());
		return checkImg;
	}
	public static void main(String[] args) throws Exception, InterruptedException, org.bytedeco.javacv.FrameRecorder.Exception {
		//recordCamera("output.MP4",25);
		recordCamera("rtmp://192.168.0.182/oflaDemo/hello",1000);
	}
 
}
