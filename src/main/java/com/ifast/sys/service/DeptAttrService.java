package com.ifast.sys.service;

import com.ifast.common.domain.ExportDemo;

import java.util.List;

/**
 * <pre>
 * 部门管理
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
public interface DeptAttrService {

	void saveDeptAndAttr(List<ExportDemo> exportDemoList);
}
