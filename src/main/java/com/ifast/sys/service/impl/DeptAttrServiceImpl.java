package com.ifast.sys.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ifast.common.domain.DictDO;
import com.ifast.common.domain.ExportDemo;
import com.ifast.common.service.DictService;
import com.ifast.common.utils.Const;
import com.ifast.sys.domain.DeptDO;
import com.ifast.sys.service.DeptAttrService;
import com.ifast.sys.service.DeptService;
import com.xiaoleilu.hutool.util.StrUtil;

/**
 * <pre>
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
@Transactional
@Service
public class DeptAttrServiceImpl implements DeptAttrService {

    @Autowired
    private DeptService deptService;

    @Autowired
    private DictService dictService;

    /**
     * 导入
     * @param exportDemoList
     */
    @Override
    public void saveDeptAndAttr(List<ExportDemo> exportDemoList){
    	QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
        wrapper.like("value","dw_");
        wrapper.isNull("parentId");
        List<DictDO> dictDOList = this.dictService.list(wrapper);
        Map<String,DictDO> mapDicPNames = Maps.newHashMap();
        for (DictDO dictDO : dictDOList) {
            mapDicPNames.put(dictDO.getName(),dictDO);
        }

        List<DeptDO> deptDOInsertList = Lists.newArrayList();
        Map<String,String> mapExcelNames = Maps.newHashMap();
        int i = 1;
        for (ExportDemo exportDemo : exportDemoList) {
            if(StrUtil.isNotBlank(exportDemo.getQymc())){
                if(!mapExcelNames.containsKey(exportDemo.getQymc())){
                    String parentId = "1";
                    DeptDO deptDO = new DeptDO();
                    deptDO.setId(Const.uuid());
                    deptDO.setDelFlag(0);
                    deptDO.setName(exportDemo.getQymc());
                    deptDO.setOrderNum(i+1);
                    deptDO.setParentId(parentId);
                    deptDO.setYb(exportDemo.getYb());
                    deptDO.setScjydz(exportDemo.getScjydz());
                    deptDO.setZjdyq(exportDemo.getZjdyq());
                    deptDO.setZyfzr(exportDemo.getZyfzr());
                    deptDO.setZyjgbm(exportDemo.getZyjgbm());
                    QueryWrapper<DictDO> wrapper1 = new QueryWrapper<>();
                    if(StrUtil.isNotBlank(exportDemo.getJjlx())){
                        wrapper1.eq("name",exportDemo.getJjlx());
                        DictDO dictDOOld = this.dictService.getOne(wrapper1);
                        if(dictDOOld == null){
                            DictDO dictDONew = this.saveDic(exportDemo.getJjlx(),mapDicPNames.get("经济类型"));
                            deptDO.setJjlxId(dictDONew.getId());
                        }
                    }
                    if(StrUtil.isNotBlank(exportDemo.getQyjyzt())){
                        wrapper1 = new QueryWrapper<>();
                        wrapper1.eq("name",exportDemo.getQyjyzt());
                        DictDO dictDOOld = this.dictService.getOne(wrapper1);
                        if(dictDOOld == null){
                            DictDO dictDONew = this.saveDic(exportDemo.getQyjyzt(),mapDicPNames.get("企业经营状态"));
                            deptDO.setQyjyztId(dictDONew.getId());
                        }else{
                            deptDO.setQyjyztId(dictDOOld.getId());
                        }
                    }
                    if(StrUtil.isNotBlank(exportDemo.getQygm())){
                        wrapper1 = new QueryWrapper<>();
                        wrapper1.eq("name",exportDemo.getQygm());
                        DictDO dictDOOld = this.dictService.getOne(wrapper1);
                        if(dictDOOld == null){
                            DictDO dictDONew = this.saveDic(exportDemo.getQygm(),mapDicPNames.get("企业规模"));
                            deptDO.setQygmId(dictDONew.getId());
                        }else{
                            deptDO.setQygmId(dictDOOld.getId());
                        }
                    }
                    if(StrUtil.isNotBlank(exportDemo.getFxdj())){
                        wrapper1 = new QueryWrapper<>();
                        wrapper1.eq("name",exportDemo.getFxdj());
                        DictDO dictDOOld = this.dictService.getOne(wrapper1);
                        if(dictDOOld == null){
                            DictDO dictDONew = this.saveDic(exportDemo.getFxdj(),mapDicPNames.get("风险等级"));
                            deptDO.setFxdjId(dictDONew.getId());
                        }else{
                            deptDO.setFxdjId(dictDOOld.getId());
                        }
                    }
                    if(StrUtil.isNotBlank(exportDemo.getHylb())){
                        wrapper1 = new QueryWrapper<>();
                        wrapper1.eq("name",exportDemo.getHylb());
                        DictDO dictDOOld = this.dictService.getOne(wrapper1);
                        if(dictDOOld == null){
                            DictDO dictDONew = this.saveDic(exportDemo.getHylb(),mapDicPNames.get("行业类别"));
                            deptDO.setQylbId(dictDONew.getId());
                        }else{
                            deptDO.setQylbId(dictDOOld.getId());
                        }
                    }
                    deptDOInsertList.add(deptDO);
                    mapExcelNames.put(exportDemo.getQymc(),exportDemo.getQymc());
                }
            }
        }
        //初始化单位
        this.deptService.saveBatch(deptDOInsertList);
    }


    public DictDO saveDic(String dictname,DictDO dictDOParent){
        DictDO dictDO = new DictDO();
        dictDO.setDelFlag("0");
        dictDO.setName(dictname);
        dictDO.setValue(Const.uuid());
        dictDO.setRemarks("");
        dictDO.setDescription("");
        dictDO.setType(dictDOParent.getValue());
        dictDO.setParentId(dictDOParent.getId());
        dictDO.setSort(BigDecimal.ONE);
        this.dictService.save(dictDO);
        return  dictDO;
    }
}
