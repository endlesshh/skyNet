package com.ifast.sys.controller;

import com.google.common.collect.Lists;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.domain.ExportDemo;
import com.ifast.common.utils.ExcelEasyUtil;
import com.ifast.sys.service.DeptAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <pre>
 * 部门管理
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@Controller
@RequestMapping("/sys/deptAttr")
public class DeptAttrController extends AdminBaseController {

    @Autowired
    private DeptAttrService deptAttrService;

    @GetMapping("/export")
    public void export() {
        /*delete from  SYS_DEPT where Parentid not in (0,1);
        delete from  SYS_DEPT_attr;
        commit;*/
        List<String> paths = Lists.newArrayList();
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\道路交通.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\非煤矿山.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\工矿商贸.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\建筑施工.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\金属冶炼.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\民爆物品.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\人员聚集.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\危险化学品.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\消防安全.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\学校安全.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\烟花爆竹.xls");
        paths.add("E:\\平安福\\各行业及企业名称\\各行业及企业名称\\制造生产.xls");
        for(String path : paths){
            List<ExportDemo> list = ExcelEasyUtil.importExcel(path,0,1,ExportDemo.class);
            System.out.println(list.size());
            this.deptAttrService.saveDeptAndAttr(list);
        }
    }



}
