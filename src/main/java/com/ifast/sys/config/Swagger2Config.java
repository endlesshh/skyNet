package com.ifast.sys.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.ifast.common.config.IFastConfig;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class Swagger2Config {

    @Autowired
    IFastConfig ifastConfig;

    @Bean
    public Docket createRestApi() {
        String projectRootURL = ifastConfig.getProjectRootURL();
        int s = projectRootURL == null ? -1 : projectRootURL.indexOf("//");
        int e = s == -1 ? -1 : projectRootURL.indexOf('/', s + 2);
        String host = s == -1 ? null : projectRootURL.substring(s + 2, e == -1 ? projectRootURL.length() : e);
        Predicate<RequestHandler> selector1 = RequestHandlerSelectors.basePackage("com.ifast.api.controller");
        Predicate<RequestHandler> selector2 = RequestHandlerSelectors.basePackage("com.ifast.api.controller.wap");
        
        return new Docket(DocumentationType.SWAGGER_2).host(host).apiInfo(apiInfo()).select()
                // 为当前包路径
                //.apis(RequestHandlerSelectors.any())
        		.apis(Predicates.or(selector1,selector2))
                .paths(PathSelectors.any()).build();
    }

    // 构建 api文档的详细信息函数
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 页面标题
                .title("平安福项目接口")
                // 创建人
                .contact(new Contact("Luculent", "luculent@luculent.net", "neimenggu@luculent.net"))
                // 版本号
                .version("1.0")
                // 描述
                .description("移动调用").build();
    }
}