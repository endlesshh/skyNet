package com.ifast.sys.domain;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * <pre>
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
@Data
@TableName("sys_user")
public class UserDO implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;
    // 用户名
    private String username;
    // 用户真实姓名
    private String name;
    // 密码
    private String password;
    // 部门
    @TableField("DEPTID")
    private String deptId;

    @TableField(exist = false)
    private String deptName;

    // 邮箱
    private String email;
    // 状态 0:禁用，1:正常
    private Integer status;
    /** 创建人 */
    @TableField(fill = FieldFill.INSERT)
    private String createId ;
    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private String createTime ;
    /** 更新人 */
    @TableField(fill = FieldFill.UPDATE)
    private String updateId ;
    /** 更新时间 */
    @TableField(fill = FieldFill.UPDATE)
    private String updateTime ;
    //角色
    @TableField(exist = false)
    private List<Long> roleIds;
    //性别
    private Integer sex;
    //图片ID
    @TableField("PICID")
    private Long picId;
    //现居住地
    @TableField("LIVEADDRESS")
    private String liveAddress;
    //爱好
    private String hobby;
    //省份
    private String province;
    //所在城市
    private String city;
    //所在地区
    private String district;
    
    @TableField("TOPDEPTID")
    private String topDeptId;

    
}
