package com.ifast.sys.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * <pre>
 * 部门管理
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
@TableName("sys_dept")
@Data
public class DeptDO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    // 上级部门ID，一级部门为0
    @TableField(value = "PARENTID")
    private String parentId;
    // 部门名称
    private String name;
    // 排序
    @TableField(value = "ORDERNUM")
    private Integer orderNum;
    // 是否删除 -1：已删除 0：正常
    @TableField(value = "DELFLAG")
    private Integer delFlag;

    // 主要负责人
    private String zyfzr;
    // 生产经营地址
    private String scjydz;
    //镇街道园区
    private String zjdyq;
    //专业监管部门
    private String zyjgbm;
    //邮编
    private String yb;
    //邮编
    @TableField(value = "CREATETIME")
    private String createTime;
    //行业类别
    @TableField(value = "QYLBID")
    private String qylbId;

    @TableField(exist = false)
    private String qylbName;

    //企业规模
    @TableField(value = "QYGMID")
    private String qygmId;
    //风险等级
    @TableField(value = "FXDJID")
    private String fxdjId;
    // 经济类型Id
    @TableField(value = "JJLXID")
    private String jjlxId;
    //企业经营状态
    @TableField(value = "QYJYZTID")
    private String qyjyztId;


}
