package com.ifast.common.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author :luzq
 * @date:2019/3/20 0020 下午 3:05
 * @copyright: @2018
 */
@Data
public class ExportDemo {

    @Excel(name = "社会统一代码", orderNum = "0")
    private String shtydm;

    @Excel(name = "企业名称",  orderNum = "1")
    private String qymc;

    @Excel(name = "主要负责人",  orderNum = "2")
    private String zyfzr;

    @Excel(name = "经济类型",  orderNum = "3")
    private String jjlx;

    @Excel(name = "生产经营地址",  orderNum = "4")
    private String scjydz;

    @Excel(name = "企业经营状态",  orderNum = "5")
    private String qyjyzt;

    @Excel(name = "镇街道园区",  orderNum = "6")
    private String zjdyq;

    @Excel(name = "行业类别",  orderNum = "7")
    private String hylb;

    @Excel(name = "专业监管部门",  orderNum = "8")
    private String zyjgbm;

    @Excel(name = "行业监管部门",  orderNum = "9")
    private String hyjgbm;

    @Excel(name = "企业规模",  orderNum = "10")
    private String qygm;

    @Excel(name = "风险等级",  orderNum = "11")
    private String fxdj;

    @Excel(name = "邮编",  orderNum = "12")
    private String yb;


}
