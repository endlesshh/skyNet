package com.ifast.common.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.google.common.collect.Maps;

/**
 * 项目名称：hongshan_app
 * 类名称：GlobalConstatUtils
 * 类描述： 
 * 创建人：ShiQiang 
 * 创建时间：2019年3月14日12:20:21
 */
@Configuration
@PropertySource("classpath:config/message.conf")
public class GlobalConstatUtils {

    private static Map<String,String> urlAndApps = Maps.newHashMap(); 
     
    @Value("${style.one}")
    private void setStyleone(String styleone){ 
    	urlAndApps.put("styleone", styleone);
    }
   
    @Value("${style.two}")
    private void setStyletwo(String styletwo){ 
    	urlAndApps.put("styletwo", styletwo);
    } 
     
    public static String getValByName(String name){ 
        return GlobalConstatUtils.urlAndApps.get(name);
    }
    
}
