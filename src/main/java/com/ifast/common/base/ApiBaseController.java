package com.ifast.common.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.ifast.api.util.JWTUtil;
import com.ifast.sys.domain.UserDO;
import com.ifast.sys.service.UserService;

/**
 * 
 * <pre>
 * </pre>
 * 
 * <small> 2018年2月25日 | Aron</small>
 */
public abstract class ApiBaseController {

    @Autowired
    private UserService userService;
    
    public UserDO getUser(String token){  
        return userService.getById(JWTUtil.getUserId(token));
    }
}