package com.ifast.common.utils;

import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.date.DatePattern;
import com.xiaoleilu.hutool.date.DateTime;
import com.xiaoleilu.hutool.date.DateUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


/**
 * 项目名称：
 * 修改日期：2015/11/2
*/
public class Const {
	
	public static final String[] aw = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T"};
	   
	public static long SMS_TIME_OUT = 60;//秒
	/**
	 * 督查考核短信APP_ID(腾讯云)
	 */
	public static final int APP_ID = 1400197895;

	/**
	 * 督查考核短信APP_KEY(腾讯云)
	 */
	public static final String APP_KEY = "b55583d77aaa662286c0b3d88ad6bbfe";
	
	/**
	 * 手机号国家码
	 */
	public static final String NATION_CODE = "86";
	
	/**
	 * 短信签名
	 */
	public static final String SIGN = "";
	
	/**
	 * 短信异常是否警报
	 */
	public static final boolean SMS_ERR_ALARM = true;
	
	/**
	 * 短信异常警报电话
	 */
	public final static ArrayList<String> SMS_ERR_PHONE = Lists.newArrayList("15247648452");
	

	//字典 党内职务

	public final static  String filePath ="F:"+ File.separator+File.separator+"imgs"+File.separator+"imgFile"+File.separator;

	
	/**
	 * 考核测评统计精确位数---小数点后两位
	 */
	public final static int scale =2;
	public final static int scale4 =4;


	/**
	 * 工作日志统计连续未填报次数
	 */
	public final static int lxwtbcs = 3;

	/**
	 * 默认密码
	 */
	public final static String password="1";

	/**
	 * 家庭成员关系gValue
	 */
	public final static String jtcyGx = "jtcygx";
	public final static String zzmm = "zzmm";

	private Const() {}
	
	/**每个相册的大小 100m*/
	public final static int everyAlbumSize = 100 * 1024 * 1024;
	/**用户所用相册的大小 1000m*/
	public final static int maxAlbumSize = 10 * 100 * 1024 * 1024;
	
	/**app 1000m*/
	public final static int maxApkSize = 10 * 100 * 1024;
	/**相册*/
	public enum album {
		album_nomal("album_nomal","DE4846A8EFE0424BA188B95BF09DB1A5","普通相册"), 
		album_loop("album_loop","44C4E33A36244568AC0199D4ADF0D71E","轮播相册");
		private String value;private String id;private String name;
		private album(String value,String id,String name) {this.value = value;this.id = id;this.name = name;} 
		public String getValue(){return value;}
		public String getName(){return name;}
		public String getId(){return id;} 
	}
	/**新闻分类*/
	public enum news{ 
		news("news","F642C63D392B46E08DB92FA5250FCC6D","新闻分类"),
		news_information("news_information","03BFCC4B73C545EAB913CFC97DCEB30F","新闻资讯"),
		news_safe("news_safe","60405B814FEB4C279EC84471BD20FB4A","安全知识"),
		news_help("news_help","78F0679A8C034EA2B5D4BAA062B83C08","使用手册"), 
		news_example("news_example","5EE11AECCE8342C99F6D4295D8C0C90C","安全案例"), 
		news_law("news_law","3C5BCE423AD844DF9D47231E53D6C0AC","法律法规"), 
		news_policy("news_policy","EF9A531BD8BA404083D17B743BD82D9C","政策文件"), 
		news_about_ours("news_about_ours","24248604370D4A4FB250B9F0F635AD48","关于我们"), 
		news_agreement("news_agreement","D9D61D5E90C14CD1A75D59AFB95CF615","服务协议"); 
		private String value;private String id;private String name;
		private news(String value,String id,String name) {this.value = value;this.id = id;this.name = name;} 
		public String getValue(){return value;}
		public String getName(){return name;}
		public String getId(){return id;} 
	}
	/**新闻类型*/
	public enum newsType{ 
		news_type_text("news_type_text","A69CAECBF07143D58471104829527B74","文本新闻"),
		news_type_picture("news_type_picture","059607FBE17D43FCB18C8BA9179C1681","图文新闻"); 
		private String value;private String id;private String name;
		private newsType(String value,String id,String name) {this.value = value;this.id = id;this.name = name;} 
		public String getValue(){return value;}
		public String getName(){return name;}
		public String getId(){return id;} 
	}
	/**积分说明类型*/
	public enum integralExplain {
		/**问答*/
		ask("integral_ask","5E55C35D29234212A38F37E6511605ED"),
		/**警告*/
		warn("integral_warn","DB45667BD98242CC80CB972AC415FA43"),
		/**规则*/
		rule("integral_rule","8219DCF720F947619FEBC8F64C2EFCAF");  
		private String value;private String id;
		private integralExplain(String value,String id){this.value = value;this.id = id;} 
		public String getValue(){return value;} 
		public String getId(){return id;} 
	}
	
	/**积分规则类型*/
	public enum integral { 
		integral_learn("integral_learn","BDCD79A381454600B2852E8450851186","学习积分"), 
		integral_read("integral_read","5EFA1D416FED4806A301F190E5CDFA88","阅读积分"), 
		integral_one_week("integral_one_week","F17D35D8F81A4C3C9B242D992CF68C2E","每周一答积分"), 
		integral_one_day("integral_one_day","68B889BFD8604428A211C97F04E88645","每日一题积分"), 
		integral_smart("integral_smart","37FEB49D7E2240F5ACFB3B31D8B20AF2","智能答题积分"), 
		integral_formal("integral_formal","A8BCA9DBED88403C84A23A76000CEC5E","专题考试积分"), 
		integral_share("integral_share","6E41AB069D784F35AFF6118F75F4113B","分享积分"), 
		integral_photo("integral_photo","74F5F89E6BF14DD0B3F13E1F65255BB8","照片上传积分"), 
		integral_login("integral_login","786247198E684680928ECB051ABEC794","登录积分");
		private String value;private String id;private String name;
		private integral(String value,String id,String name) {
			this.value = value;this.id = id;this.name = name;
		}
		public String getValue(){return value;}
		public String getId(){return id;}
		public String getName(){return name;}
		public static integral getIntegralBySmartExamId(String id){
			integral type = integral_one_day;
	    	if(smartExam.one_day_exam.getId().equals(id)){
	    		type = integral_one_day; 
	    	}else if(smartExam.one_week_exam.getId().equals(id)){
	    		type = integral_one_week;
	    	}else if(smartExam.any_time_exam.getId().equals(id)){
	    		type = integral_smart;
	    	}else if(smartExam.formal_exam.getId().equals(id)){
	    		type = integral_formal;
	    	} 
	    	return type;
		}  
	}
	
	/**积分是否开启*/
	public enum integralOpen {
		no(0),yes(1); 
		private int value;
		private integralOpen(int value){this.value = value;}
		public int getValue(){return value;}
	}
	
	/** 试卷类型*/
	public enum smartExam {
		/**每日一题*/
		one_day_exam("one_day_exam","E3485CD63E944B5F9F2DC9CB9F9C6C91",1),
		/**每周一题*/
		one_week_exam("one_week_exam","32CABEB6A13748BFB7AB97133C117849",5),
		/**智能答题*/
		any_time_exam("any_time_exam","03B0068FE2F54028A8A20C4FFB1E3463",5),
		/**专题考试答题*/
		formal_exam("formal_exam","D519D15A83284C29824E26707FA8DE85",0);
		
		private String value;private String id;private int count;
		private smartExam(String value,String id,int count) {
			this.value = value;this.id = id;this.count = count;
		}
		public String getValue(){return value;}
		public String getId(){return id;}
		public int getCount(){return count;}
	}
	/**类型*/
	public enum DelEnum {
		/**未删除*/
		no("0"),
		/**已删除*/
		yes("1"); 
		private String value;
		private DelEnum(String value){this.value = value;}
		public String getValue(){return value;}
	}

	public enum scoringType{
		/**正在计算*/
		scoring("1"),
		/**计算结束*/
		scoringEnd("0");
		private String  value;
		scoringType(String value){this.value =value;}
		public String getValue(){return value;}
	}
	/**
	 * 考核测评答案回传状态
	 * 考试版本同步状态
	 */
	public enum answerStatus {
		/**
		 * 未同步
		 */
		wtb(0),
		/**
		 * 同步中
		 */
		tbz(1),
		/**
		 * 已同步
		 */
		ytb(2),
		/**
		 * 失败
		 */
		sb(3);

		private int value;
		answerStatus(int value){
			this.value = value;}
			public int getValue(){
				return value;}
	}

	public enum CancelType{
		//未作废
		on("0"),
		yes("1");
		private String value;
		private CancelType(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}
	/** 是否被应用（使用）*/
	public enum QuoteType{
		/**未被引用（使用）*/
		on("0"),
		/**已被使用*/
		yes("1");
		private String value;
		QuoteType(String value){
			this.value = value;
		}
		public String getValue(){
			return value;
		}
	}
	/**
	 *  类型   0 单位  1人员
	 */
	public enum DwOrRyEnum {

		dw("0"),
		ry("1");

		private String value;

		DwOrRyEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}


	/**
	 *  类型   0 1单位多人  1多单位多人  2 单位
	 */
	public enum KhcpTypeEnum {

		zero(0),
		one(1),
		two(2);

		private Integer value;

		KhcpTypeEnum(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return value;
		}
	}



	public static String uuid(){
		return UUID.randomUUID().toString().trim().replaceAll("-", "");
	}

	public static String version(){
		DateTime dateTime = new DateTime(DateUtil.now(), DatePattern.NORM_DATETIME_FORMAT);
		Integer month = dateTime.month()+1;
		String monthStr;
		if(month < 10){
			monthStr = "0"+month;
		}else{
			monthStr = month+"";
		}
		Integer day =  dateTime.dayOfMonth();
		String dayStr;
		if(day < 10){
			dayStr = "0"+day;
		}else{
			dayStr = day+"";
		}
		return dateTime.year()+""+monthStr+""+dayStr;
	}

	public static String get32UUID(){
		return UUID.randomUUID().toString().trim().replaceAll("-", "");
	}

	/**
	 * 获取季度
	 * @param dateStr
	 * @return
	 */
	public static int season(Date dateStr){
		return DateUtil.month(dateStr)/3+1;
	}

	public enum wlcpRepeat{
		norepeat("0"),
		repeat("1");
		private String value;

		wlcpRepeat(String value) {
			this.value = value;
		}
		public String getValue(){
			return this.value;}
	}

	/**
	 * 网络测评是否同步试卷到网络测评服务器
	 */
	public enum wlcpIsUpload{

		yes("1"),
		no("0");
		private String value;
		wlcpIsUpload(String value){
			this.value = value;
		}
		public String getValue(){
			return this.value;}
	}

	public enum khcpIsNode{
		/**
		 * 一级类型
		 */
		first("0"),
		/**
		 * 二级类型
		 */
		second("1");
		private String value;
		khcpIsNode(String value){
			this.value = value;
		}
		public String getValue(){
			return this.value;
		}
	}

	/**
	 * 试题类型
	 */
	public enum khcpStlx{
		/**
		 * 普通类型测评票
		 */
		ptcpp("stlx_pt"),
		/**
		 * 领导行为特征测评票
		 */
		ldcpp("stlx_ld"),
		/**
		 * 其他意见票
		 */
		qtcpp("stlx_qt"),
		/**
		 * 字典项分组值
		 */
		fzz("stlx");
		private String value;
		khcpStlx(String value){
			this.value = value;
		}
		public String getValue(){
			return this.value;
		}
	}

	/**
	 * 试题类型
	 */
	public enum khcpIsUse{
		/**
		 * 已被使用
		 */
		yes("true"),
		/**
		 * 未被使用
		 */
		no("false");
		private String value;
		khcpIsUse(String value){
			this.value = value;
		}
		public String getValue(){
			return this.value;
		}
	} 
	
	public enum pwdDefault{
		/**
		 * 默认
		 */
		yes("0"),
		/**
		 * 已改过
		 */
		no("1");
		private String value;
		pwdDefault(String value){
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}

	/**
	 *是否正确选项
	  * @return
	 * @author panxy
	 * @date 2019/1/3 11:13
	 */
    public enum ExamOptTrue{
        /**
         * 正确
         */
        yes(0),
        /**
         * 错误
         */
        no(1);
        private Integer value;
        ExamOptTrue(Integer value){
            this.value = value;
        }
        public Integer getValue() {
            return value;
        }
    }
    
    /**
	 * 考试系统 题目类别
	 */
	public enum examTmfl {
		/** 单选题*/
		danxuancpp("exam_danxuanti","79F25458F2A94266BF6E961F3DB34E7D","单选题"),
		/*** 多选题*/
		duoxuancpp("exam_duoxuanti","6432B036F5304BA2A5762E2E8967A9D2","多选题"),
		/** 判断题*/
		panduancpp("exam_panduanti","D6AB9BBA8AB948D5B0884E1B468595F7","判断题"),
		/*** 字典项分组值*/
		fzz("exam_tmfl","CADFAE25BA034D2BB0259AFA920B27D0","试题类型");
		private String value;
		private String id;
		private String name;
		
		examTmfl(String value,String id,String name){
			this.value = value;
			this.id = id;
			this.name = name;
		}
		public static String getName(String value){
			if(danxuancpp.value.equals(value)){
				return danxuancpp.name;
			}else if(duoxuancpp.value.equals(value)){
				return duoxuancpp.name;
			}else if(panduancpp.value.equals(value)){
				return panduancpp.name;
			}else{
				return "";
			} 
		}
		public String getValue(){
			return this.value;
		} 
		public String getName() {
			return name;
		}
		public String getId() {
			return id;
		}
	} 
	 
	public enum XxcjDictStatus{
		/*待提交*/
		dtj(0),
		/*待审核*/
		dsh(1),
		/*审通过*/
		shtg(2),
		/*审核失败*/
		shsb(3),
		/*同步失败*/
		tbsb(4);
		private Integer value;
		public Integer getValue() {
			return value;
		}
		XxcjDictStatus(Integer value) {
			this.value = value;
		}
	}

	public enum XxcjShStatus{
		/*0：联络人保存，1：提交审核，2：审核通过，3：退回编辑*/
		llrbc("0"),
		tjsh("1"),
		shtg("2"),
		thbj("3");
		private String value;
		public String getValue() {
			return value;
		}
		XxcjShStatus(String value) {
			this.value = value;
		}
	}
	public enum XxcjBjStatus{
		/** 申请编辑状态;0：无操作 1：申请编辑，2：允许编辑，3：不允许编辑 */
		wucaozuo("0"),
		shenQingBianJi("1"),
		yunXuBianJi("2"),
		buYunXu("3");
		private String value;
		public String getValue() {
			return value;
		}
		XxcjBjStatus(String value) {
			this.value = value;
		}
	}

	public enum GbxxDictState{
		//0:系统内；1：信息采集同步
		xiTongNei("0"),
		xxcj("1");
		private String value;
		public String getValue(){
			return value;}

		GbxxDictState(String value) {
			this.value = value;
		}
	}
	public enum  JllqkshState{
		/*0：保存，1：提交*/
		bc("0"),
		tj("1");
		private String value;
		public String getValue() {
			return value;
		}
		JllqkshState(String value) {
			this.value = value;
		}
	}
	/**
	 *  短信模板ID
	 */
	public enum MessageTemplateId {
		/**
		 * 任务下发
		 */
		TASK_SEND(172921),
		/**
		 * 任务汇报
		 */
		TASK_REPORT(119564),
		/**
		 * 任务未汇报提醒
		 */
		TASK_NOT_REPORT(172922),
		/**
		 * 任务未汇报重复提醒
		 */
		TASK_NOT_REPORT_REPEAT(172923),
		/**
		 * 任务汇报退回
		 */
		TASK_REPORT_RETURN(119568),
		/**
		 * 文件下发
		 */
		FILE_SEND(172924),
		/**
		 * 任务延期申请
		 */
		TASK_DELAY_APPLY(119571),
		/**
		 * 任务进入审核期
		 */
		TASK_AUDIT_PERIOD(119573),
		/**
		 * 监控下发审核任务
		 */
		JKBM_VERIFY_TASK(119574),
		/**
		 * 发送短信错误
		 */
		SEND_SMS_ERR(127426);
		
		private int value;
		
		private MessageTemplateId(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
}
