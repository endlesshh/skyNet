package com.ifast.face.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ifast.common.base.BaseDao;
import com.ifast.face.domain.UserFaceImg;

@Transactional
public interface UserFaceImgDao extends BaseDao<UserFaceImg>{}
