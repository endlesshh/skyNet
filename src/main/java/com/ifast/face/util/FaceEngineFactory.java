package com.ifast.face.util;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import com.arcsoft.face.EngineConfiguration;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FunctionConfiguration;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;

/**
 * 识别引擎
 * @author ShiQiang
 *
 */
public class FaceEngineFactory extends BasePooledObjectFactory<FaceEngine> {

    private String appId;
    private String sdkKey;
    private FunctionConfiguration functionConfiguration;
    private Integer detectFaceMaxNum=10;
    private Integer detectFaceScaleVal=16;
    private DetectMode detectMode= DetectMode.ASF_DETECT_MODE_IMAGE;
    private DetectOrient detectFaceOrientPriority= DetectOrient.ASF_OP_0_HIGHER_EXT;


    public FaceEngineFactory(String appId, String sdkKey, FunctionConfiguration functionConfiguration) {
        this.appId = appId;
        this.sdkKey = sdkKey;
        this.functionConfiguration = functionConfiguration;
    }



    @Override
    public FaceEngine create() throws Exception {

        EngineConfiguration engineConfiguration= EngineConfiguration.builder()
                .functionConfiguration(functionConfiguration)
                .detectFaceMaxNum(detectFaceMaxNum)
                .detectFaceScaleVal(detectFaceScaleVal)
                .detectMode(detectMode)
                .detectFaceOrientPriority(detectFaceOrientPriority)
                .build(); 
        return engineBuild(engineConfiguration);
    }

    private synchronized FaceEngine engineBuild(EngineConfiguration engineConfiguration){
    	 FaceEngine faceEngine =new FaceEngine();
         //此处调用本地文件，会引发多线程问题
    	 faceEngine.active(appId,sdkKey);
         faceEngine.init(engineConfiguration);

         return faceEngine;
    }
    
    @Override
    public PooledObject<FaceEngine> wrap(FaceEngine faceEngine) {
        return new DefaultPooledObject<>(faceEngine);
    }


    @Override
    public void destroyObject(PooledObject<FaceEngine> p) throws Exception {
        FaceEngine faceEngine = p.getObject();
        //int result = 
        faceEngine.unInit();
        super.destroyObject(p);
    }
}
