package com.ifast.face.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class WebCamInfo {

    private String webCamName;
    private String webCamIndex;
 
    @Override
    public String toString(){
        return webCamName;
    }
}