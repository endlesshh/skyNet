package com.ifast.face.controller;


import java.net.MalformedURLException;

import javax.swing.JFrame;

import org.springframework.stereotype.Controller;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.ds.ipcam.IpCamAuth;
import com.github.sarxos.webcam.ds.ipcam.IpCamDevice;
import com.github.sarxos.webcam.ds.ipcam.IpCamDeviceRegistry;
import com.github.sarxos.webcam.ds.ipcam.IpCamDriver;
import com.github.sarxos.webcam.ds.ipcam.IpCamMode;
import com.ifast.common.base.AdminBaseController;


@Controller
public class IpCamController extends AdminBaseController {

    static {
    	Webcam.setDriver(new IpCamDriver());
    }

    public static void main(String[] args) throws MalformedURLException {
    	IpCamDeviceRegistry.register(new IpCamDevice("Lignano", "http://192.168.0.16:8181/", IpCamMode.PUSH,new IpCamAuth("admin","admin")));
    	JFrame f = new JFrame("Live Views From Lignano Beach");
    	f.add(new WebcamPanel(Webcam.getDefault()));
    	f.pack();
    	f.setVisible(true);
    	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    
}
