package com.ifast.face.service;

import java.awt.image.BufferedImage;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ifast.face.domain.UserFaceInfo;


public interface UserFaceInfoService extends IService<UserFaceInfo>{

    UserFaceInfo saveFace(UserFaceInfo userFaceInfo);
    
    void saveFace(BufferedImage img,String name,Long groupId,Integer gender,Integer age)throws Exception;
    
    UserFaceInfo findByFaceId(String groupId);
    
    void deleteInBatch(List<UserFaceInfo> users);
    
    IPage<UserFaceInfo> selectPage(Page<UserFaceInfo> page);
    
    IPage<UserFaceInfo> selectPage(Page<UserFaceInfo> page, Wrapper<UserFaceInfo> wrapper);
}
