package com.ifast.face.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ifast.face.dao.UserFaceInfoDao;
import com.ifast.face.domain.UserFaceInfo;
import com.ifast.face.dto.FaceUserInfo;
import com.ifast.face.dto.ProcessInfo;
import com.ifast.face.service.FaceEngineService;
import com.ifast.face.service.UserFaceInfoService;
import com.ifast.face.util.ImageInfo;
import com.ifast.face.util.ImageUtil;
import com.ifast.face.util.WorkId;
import com.xiaoleilu.hutool.date.DateUtil;

import cn.hutool.core.collection.CollectionUtil;


@Service("userFaceInfoService")
public class UserFaceInfoServiceImpl extends ServiceImpl<UserFaceInfoDao, UserFaceInfo> implements UserFaceInfoService {


	@Value("${config.file.save.path}")
    public String fileSavePath;
    
    @Autowired
    private FaceEngineService        faceEngineService; 
   
	@Autowired
    private UserFaceInfoDao userFaceInfoMapper;


    @Override
    public void saveFace(BufferedImage img,String name,Long groupId,Integer gender,Integer age) throws Exception{
        String faceId = WorkId.sortUID(); 
        File outputfile = new File(fileSavePath+ File.separator + faceId + ".png");
        //ImageIO.write(img, "png", outputfile);
        
        ImageInfo imageInfo = ImageUtil.bufferedImage2ImageInfo(img);
        if(imageInfo == null) return ; 
        // 人脸特征获取
        List<ProcessInfo> features = faceEngineService.extractFaceFeature (imageInfo);
        if (CollectionUtil.isEmpty(features)) return;
        
        for(ProcessInfo feature : features){
           
            // 人脸特征插入到数据库 
        	UserFaceInfo userInfo = saveFace(UserFaceInfo.builder().imgPath(outputfile.getAbsolutePath()).createTime(DateUtil.now()).checked(0).gender(gender).age(age).name(name).groupId(groupId).faceFeature (feature.getFaceFeature().getFeatureData()).faceId(faceId).build());
            // 人脸信息添加到缓存 
            faceEngineService.addFaceToCache(groupId, FaceUserInfo.builder().id(userInfo.getId()).faceFeature (feature.getFaceFeature().getFeatureData()).name(name).faceId(faceId).build());
          
        }
    }

    @Override
    public UserFaceInfo findByFaceId(String groupId){
        return userFaceInfoMapper.findByFaceId(groupId);
    }

	@Override
	public UserFaceInfo saveFace(UserFaceInfo userFaceInfo) {
		userFaceInfoMapper.insert(userFaceInfo);
		return userFaceInfo;
	}
	
	@Override
    public void deleteInBatch(List<UserFaceInfo> users){ 
		userFaceInfoMapper.deleteBatchIds(users.stream().map(user -> user.getId()).collect(Collectors.toList()));
    }
	
	@Override
	public IPage<UserFaceInfo> selectPage(Page<UserFaceInfo> page){
		IPage<UserFaceInfo> users = super.page(page);
		users.getRecords().stream().forEach(user -> {user.setGenderName((new Integer(1)).equals(user.getGender())?"男":"女");
		user.setCheckState((new Integer(1)).equals(user.getChecked())?"已检测到":"未检测到");});
		return users;
	} 
    @Override
	public IPage<UserFaceInfo> selectPage(Page<UserFaceInfo> page, Wrapper<UserFaceInfo> wrapper) {
    	IPage<UserFaceInfo> users = super.page(page, wrapper);
    	users.getRecords().stream().forEach(user -> {user.setGenderName((new Integer(1)).equals(user.getGender())?"男":"女");
		user.setCheckState((new Integer(1)).equals(user.getChecked())?"已检测到":"未检测到");});
		return users;
	}
}
