package com.ifast.face.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@TableName("USER_FACE_INFO")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UserFaceInfo {

	@TableId(type= IdType.AUTO)
    private Long id;

    private Long groupId;

    private String faceId;

    private String name;

    private Integer age;

    private String email;

    private Integer gender;

    private Integer checked;//0未检测，1已经检测到
    
    private String phoneNumber; 

    private byte[] faceFeature; 
    
    /** 创建人 */ 
    private String createId ;
    /** 创建时间 */ 
    private String createTime ;
    /** 更新人 */ 
    private String updateId ;
    /** 更新时间 */ 
    private String updateTime ;
    
    private String imgPath;
    
    private String pathType;//路径格式  本地还是网络
    @TableField(exist = false)
    private String genderName;
    @TableField(exist = false)
    private String checkState;
    
}