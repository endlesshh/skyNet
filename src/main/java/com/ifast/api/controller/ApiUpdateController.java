package com.ifast.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ifast.common.domain.Update;
import com.ifast.common.service.UpdateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;

/**
 * @ClassName ApiUpdateController
 * @Author ShiQiang
 * @Date 2019年4月24日10:18:42
 * @Version v1.0
 */
@RestController
@Api(description = "app更新")
@RequestMapping("/api/update/")
public class ApiUpdateController {

    @Autowired
    private UpdateService updateService;

     
    @RequestMapping(value = "updateApp.do")
    @ApiOperation( value = "APP更新", httpMethod = "GET",notes="参数type:标识")
    @ApiResponse(code = 200, message = "success", response = Update.class) 
    public Update updateApp(
    		@ApiParam(name = "type", required = true, value = "值：（iso,android）") @RequestParam(value = "type",required=true) String type){
    	try { 
    		return updateService.getUpdate(type);
    	} catch (Exception e) { 
			e.printStackTrace(); 
			return null;
		}
    } 
}
