package com.ifast.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ifast.api.service.UserService;
import com.ifast.api.util.DataResult;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <pre>
 *  基于jwt实现的API测试类
 * </pre>
 * 
 * <small> 2018年4月27日 | Aron</small>
 */
@RestController
@RequestMapping("/api/login/")
public class ApiLoginController {

    @Autowired
    private UserService userService;

    @PostMapping("login")
    @ApiOperation("登录")
    public DataResult<?> token(@RequestParam String userName, @RequestParam String passWord) {  
        return DataResult.ok().put("token",userService.getToken(userName, passWord).getToken());
    }

    /*@GetMapping("refreshToken")
    @ApiOperation("刷新token")
    public DataResult refreshToken(@RequestParam String userName,
                            @ApiParam(name = "refreshToken", required = true, value = "refreshToken") @RequestHeader("refreshToken") String refreshToken) {
        DataResult  result = new DataResult();
        try {
            TokenVO token = userService.refreshToken(userName, refreshToken);
            result.put("Authorization",token);
        }catch (IFastApiException e){
            result.setMsg(EnumErrorCode.apiAuthorizationFailed.getMsg());
            result.setCode(EnumErrorCode.apiAuthorizationFailed.getCode());
        }
        return result;
    }*/

    @PostMapping("logout")
    @ApiOperation("退出登录")
    public DataResult<?> logout(@ApiParam(name = "Authorization", required = true, value = "token") @RequestHeader("Authorization") String token) {
        userService.logoutToken(token,"");
        return DataResult.ok();
    }

}
