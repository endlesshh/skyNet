package com.ifast.api.util;

/**
 * 
 *@Description:操作返回结果对象
 *@Author:jingWenguang
 *@Since:2016年7月28日上午8:58:10
 */
public class Result<T>{

	public final static Integer CODE_SUCCESS = 200;
    public final static Integer CODE_FAIL = -200;
    public final static Integer TOKEN_NO = 300;
    public final static Integer YWERROR_NO = 500;
    public final static String MSG_SUCCESS = "操作成功";
    public final static String MSG_FAIL = "操作失败";

    // 响应业务状态 0 成功， 1失败
    Integer code;

    // 响应消息
    String msg;

    public Result() {
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
