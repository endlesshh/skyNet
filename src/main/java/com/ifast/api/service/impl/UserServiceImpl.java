package com.ifast.api.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ifast.api.config.JWTConfig;
import com.ifast.api.exception.IFastApiException;
import com.ifast.api.exception.IFastApiUserException;
import com.ifast.api.pojo.vo.TokenVO;
import com.ifast.api.service.UserService;
import com.ifast.api.util.JWTUtil;
import com.ifast.common.base.CoreServiceImpl;
import com.ifast.common.config.CacheConfiguration;
import com.ifast.common.config.IFastConfig;
import com.ifast.common.type.EnumErrorCode;
import com.ifast.common.utils.MD5Utils;
import com.ifast.common.utils.SpringContextHolder;
import com.ifast.sys.dao.UserDao;
import com.ifast.sys.domain.UserDO;

/**
 * <pre>
 * </pre>
 * 
 * <small> 2018年4月27日 | Aron</small>
 */
@Service
public class UserServiceImpl extends CoreServiceImpl<UserDao, UserDO> implements UserService {
	/** Holder for lazy-init */
	private static class Holder {
		static final JWTConfig jwt = SpringContextHolder.getBean(IFastConfig.class).getJwt();
		static final Cache logoutTokens = CacheConfiguration.dynaConfigCache("tokenExpires", 0, jwt.getRefreshTokenExpire(), 1000);
		static {
			JWTUtil.mykey = jwt.getUserPrimaryKey();
		}
	}

	@Override
    public TokenVO getToken(String uname, String passwd) {
        UserDO user = getOne(new QueryWrapper<UserDO>().eq("username", uname));
        if (null == user || !user.getPassword().equals(MD5Utils.encrypt(uname, passwd))) {
            throw new IFastApiUserException(EnumErrorCode.apiAuthorizationLoginFailed.getCodeStr());
        }
        return createToken(user);
    }


    @Override
	public boolean verifyToken(String token, boolean refresh) {
    	String userId = null; UserDO user = null;
    	return StringUtils.isNotBlank(token)
    			&& (userId=JWTUtil.getUserId(token))!=null
    			&& notLogout(token)
    			&& (user=refresh?getOne(new QueryWrapper<UserDO>().eq("username", userId)):getById(userId))!=null
    			&& (refresh ? JWTUtil.verify(token, user.getUsername(), user.getId()+user.getPassword()) : JWTUtil.verify(token, userId, user.getUsername()+user.getPassword()));
	}


	@Override
	public TokenVO refreshToken(String uname, String refreshToken) {
		UserDO user = null;
		if(StringUtils.isNotBlank(refreshToken)
				&& uname.equals(JWTUtil.getUserId(refreshToken))
				&& notLogout(refreshToken)
				&& (user=getOne(new QueryWrapper<UserDO>().eq("username", uname)))!=null
				&& JWTUtil.verify(refreshToken, user.getUsername(), user.getId()+user.getPassword())) {

			TokenVO vo = new TokenVO();
			vo.setToken(JWTUtil.sign(user.getId() + "", user.getUsername() + user.getPassword(), Holder.jwt.getExpireTime()));
			vo.setTokenExpire(Holder.jwt.getExpireTime());
			vo.setRefleshToken(refreshToken);
			vo.setRefreshTokenExpire(Holder.jwt.getRefreshTokenExpire());
			return vo;
		}
    	throw new IFastApiException(EnumErrorCode.apiAuthorizationFailed.getCodeStr());
	}


	@Override
	public void logoutToken(String token, String refreshToken) {
		Holder.logoutTokens.putIfAbsent(token, null);
	}

	private TokenVO createToken(UserDO user) {
        TokenVO vo = new TokenVO();
		vo.setToken(JWTUtil.sign(user.getId() + "", user.getUsername() + user.getPassword(), Holder.jwt.getExpireTime()));
		vo.setRefleshToken("");
		vo.setTokenExpire(Holder.jwt.getExpireTime());
		vo.setRefreshTokenExpire(Holder.jwt.getRefreshTokenExpire());
        return vo;
	}
	
	private boolean notLogout(String token) {
		if(Holder.logoutTokens.get(token)!=null){
			throw new IFastApiException(EnumErrorCode.apiAuthorizationFailed.getMsg());
		}
		return true;
	}
}
