package com.ifast.akka.actor;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ifast.akka.model.Message;
import com.ifast.akka.service.BusinessService;
import com.ifast.face.domain.UserFaceImg;
import com.ifast.face.service.UserFaceImgService;

import akka.actor.UntypedActor;

@Component("workerActor")
@Scope("prototype")
public class WorkerActor extends UntypedActor {

    @Autowired
    private BusinessService businessService;

    @Autowired
	private UserFaceImgService userFaceImgService;
    
    final private CompletableFuture<Message> future;

    public WorkerActor(CompletableFuture<Message> future) {
        this.future = future;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        businessService.perform(this + " " + message); 
        List<UserFaceImg> imgs = userFaceImgService.list();
        if (message instanceof Message) {
        	Message msg = (Message) message;
        	msg.setFaces(imgs);
            future.complete(msg);
        } else {
            unhandled(message);
        }

        getContext().stop(self());
    }
}
