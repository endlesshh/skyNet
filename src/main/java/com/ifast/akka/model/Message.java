package com.ifast.akka.model;

import java.util.List;

import com.ifast.face.domain.UserFaceImg;

import lombok.ToString;
@ToString
public class Message {

    final private String payload;
    final private long id;
    private List<UserFaceImg> faces; 
    
    public Message(String payload, long id) {
        this.payload = payload;
        this.id = id;
    }
    
    public Message(String payload, long id,List<UserFaceImg> faces) {
        this.payload = payload;
        this.id = id;
        this.setFaces(faces);
    }
    
    public String getPayload() {
        return payload;
    }

    public long getId() {
        return id;
    }

//    @Override
//    public String toString() {
//        return "Message{" + "payload='" + payload + '\'' + ", id=" + id + '}';
//    }

	public List<UserFaceImg> getFaces() {
		return faces;
	}

	public void setFaces(List<UserFaceImg> faces) {
		this.faces = faces;
	}
}
