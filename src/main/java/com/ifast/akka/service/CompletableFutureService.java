package com.ifast.akka.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifast.akka.di.SpringExtension;
import com.ifast.akka.model.Message;
import com.ifast.face.service.UserFaceImgService;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

@Service
public class CompletableFutureService {

    @Autowired
    private ActorSystem actorSystem; 

    @Autowired
    private SpringExtension springExtension;

    public CompletableFuture<Message> get(String payload, Long id) {
        CompletableFuture<Message> future = new CompletableFuture<>();
        ActorRef workerActor = actorSystem.actorOf(springExtension.props("workerActor", future), "worker-actor");
        workerActor.tell(new Message(payload, id), null);
        return future;
    }
}
