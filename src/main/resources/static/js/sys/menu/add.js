$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});

function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/sys/menu/saveMenu",
		data : $('#signupForm').serialize(), // 你的formid
		async : false,
		error : function(request) {
            alertMsgTop("网络超时");
		},
		success : function(data) {
			if (data.code == 0) {
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
				parent.search();
			} else {
				alertMsgTop(data.msg);
			}
		}
	});
}

function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
            menuName : {
				required : true
			}
		},
		messages : {
            menuName : {
				required : icon + "请输入菜单名称"
			}
		}
	})
}