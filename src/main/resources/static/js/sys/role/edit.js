/*$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		update();
	}
});*/
function update() {
    if ($("#name").val()===""||typeof $("#name").val() ==="undefined") {
        layer.tips('请输入角色名称','#name',{
            tips: 3
        });
        $("#name").focus();
        return false;
    }
	$.ajax({
		cache : true,
		type : "POST",
		url : "/sys/role/update",
		data : $('#signupForm').serialize(),
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
				parent.search();
			} else {
                alertMsgTop(data.msg);
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            roleName : {
                required : true
            }
        },
        messages : {
            roleName : {
                required : icon + "请输入角色名称"
            }
        }
    })
}